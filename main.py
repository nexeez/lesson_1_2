#!/usr/bin/env python3
__author__ = 'Kacper Nyk'
__email__ = 'szkolatechi@op.pl'

start: str = str("Welcome!")

age: int = int(21)
height: float = float(1.81)
name: str = str("Kacper Nyk")

print(start)
print("Name:", name)
print("Age:", age)
print("Height:", height, "m")
